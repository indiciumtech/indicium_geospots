"""Main module."""

from geospots.sector_maps import query_sectors, summarize_sectors
from geospots.helpers import geojson_to_df
from geospots.openrouteservice import return_iso

load_statistics = query_sectors
load_isochrones = return_iso
summarize_statistics = summarize_sectors
