## Functions to translate geometries for postgis
from sqlalchemy import create_engine

def create_db_engine(user, pwd):
    
    db_cred = "postgresql://" + user + ":" + pwd + '@189.4.67.176:5433/indicium_company'
    engine=create_engine(db_cred)

    return engine

def geojson_to_wkt(engine, gjson):
    """ This function converts a geojson to wkt format using postgis 
    """

    # Build statement
    clean_gjson = str(gjson).replace("'", '"')
    statement="SELECT ST_AsText(ST_GeomFromGeoJSON('" + clean_gjson + "'))"

    con=engine.connect()

    result_proxy=con.execute(statement)
    result_set=result_proxy.fetchall()

    output=result_set[0][0]

    return output


def st_make_envelope(bbox):
    """ This function wraps a bbox with postgis' ST_MakeEnvelope function 

    :param bbox: Bounding box in the format of [xmin, ymin, xmax, ymax], where x=lon and y=lat  
    :type list of coordinates """

    bbox_str = [str(x) for x in bbox]

    pgis_bbox = "ST_MakeEnvelope(" + ",".join(bbox_str) + ")"

    return pgis_bbox


def st_intersects(
    geom,
    table="dev_sector_maps",
    geom_col="geom"
):
    """ Builds the intersection condition for the given geometry (in wkt or wkb format) with the geometry column 

    :param geom: string of geometry in wkt format
    :type string """

    geo_col = table + "." + geom_col

    pgis_intersects = "ST_Intersects(" + geo_col + ", ST_GeomFromText('" + geom + "', 4326))"

    return pgis_intersects

