from geopandas import read_file


def geojson_to_df(json):

    df = read_file(json)

    return df


def w_avg(group, avg_name, weight_name):
    """ http://stackoverflow.com/questions/10951341/pandas-dataframe-aggregate-function-using-multiple-columns
        In case there is no weight or t evaluates to zero, return the simple average
    """
    dimension = group[avg_name]
    weight = group[weight_name]
    try:
        return (dimension * weight).sum() / weight.sum()
    except ZeroDivisionError:
        return dimension.mean()
