========
Geospots
========



.. image:: https://img.shields.io/pypi/v/geospots.svg
        :target: https://pypi.python.org/pypi/geospots

.. image:: https://img.shields.io/travis/pedroportelas/geospots.svg
        :target: https://travis-ci.com/pedroportelas/geospots

.. image:: https://readthedocs.org/projects/geospots/badge/?version=latest
        :target: https://geospots.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

Geospots contains functions for working with spatial data. Developed for SpotsApp support and standalone usage.

Geospots relies on the Open Route Service API (https://openrouteservice.org/), a crowd sourced service which provides spatial data directly from Open Street Map.
To make full use of geospots, you will need a free API key from ORS: https://openrouteservice.org/dev/#/signup  


* Documentation: https://geospots.readthedocs.io.


Features
--------

Isochrones: Using the return_iso function, input a coordinate and driving mode to determine which areas you can reach from a spot in given times

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
